package com.example.test_vivaorganica.LocalModel;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class cityRealmModel extends RealmObject {

    @PrimaryKey
    private int idModel;
    String body;

    public cityRealmModel() {
    }

    public cityRealmModel(int idModel, String body) {
        this.idModel = idModel;
        this.body = body;
    }

    public int getIdModel() {
        return idModel;
    }

    public void setIdModel(int idModel) {
        this.idModel = idModel;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
