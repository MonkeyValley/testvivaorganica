package com.example.test_vivaorganica.LocalModel;

import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.cityModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.itemListModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.itemWeatherModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.mainModel;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class weatherModel extends RealmObject {

    @PrimaryKey
    private int id;

    String cod, list, city, date;
    int message, cnt;

    public weatherModel() {
    }

    public weatherModel(int id, String cod, String list, String city, int message, int cnt, String date) {
        this.id = id;
        this.cod = cod;
        this.list = list;
        this.city = city;
        this.message = message;
        this.cnt = cnt;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
