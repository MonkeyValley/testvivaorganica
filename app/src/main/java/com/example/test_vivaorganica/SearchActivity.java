package com.example.test_vivaorganica;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.test_vivaorganica.Adapters.ReciclerViewAdapterDays;
import com.example.test_vivaorganica.Adapters.RecyclerViewAdapterHeader;
import com.example.test_vivaorganica.Adapters.RecyclerViewAdapterSerachOffline;
import com.example.test_vivaorganica.Adapters.RecyclerViewSearch;
import com.example.test_vivaorganica.Helpers.MainCallback;
import com.example.test_vivaorganica.Helpers.SearchCallback;
import com.example.test_vivaorganica.Helpers.SearchOfflineCallback;
import com.example.test_vivaorganica.LocalModel.cityRealmModel;
import com.example.test_vivaorganica.LocalModel.weatherModel;
import com.example.test_vivaorganica.Retrofit.GetCities.InterfaceJsonCities;
import com.example.test_vivaorganica.Retrofit.GetCities.Model.JsonResponse;
import com.example.test_vivaorganica.Retrofit.GetCityByName.InterfaceJsonCityName;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.InterfaceJsonWeatherLatLon;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.cityModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.getWeatherModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.itemListModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import okio.Utf8;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity implements  SearchCallback, SearchOfflineCallback,InternetConnectivityListener {

    private RecyclerView recyclerView;
    private RecyclerViewSearch recyclerViewSearch;
    private RecyclerViewAdapterSerachOffline recyclerViewSearchOffline;
    private EditText txtSearch;
    private ImageButton btnClose;
    private TextView lblGuardados;
    private Button btnSearch;
    private LinearLayout linearLayout;

    ProgressDialog progress;
    int i = 1;
    List<JsonResponse> list = new ArrayList<>();
    List<JsonResponse> listSend = new ArrayList<>();
    Gson gson;
    InternetAvailabilityChecker mInternetAvailabilityChecker;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        progress = ProgressDialog.show(this, "Cargando...", "Porfavor espere.", true,false);

        gson = new Gson();
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        recyclerView =  findViewById(R.id.recycler_view_search);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        txtSearch = findViewById(R.id.txtSearch);
        lblGuardados = findViewById(R.id.lblOffline);
        btnClose = findViewById(R.id.btnClose);
        btnSearch = findViewById(R.id.btnSearchCityUnknow);
        linearLayout = findViewById(R.id.layerCityUnknow);

        InternetAvailabilityChecker.init(this);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()>0){
                    findChar(s.toString().substring(0, 1).toUpperCase() + s.toString().substring(1).toLowerCase());
                }else{
                    findChar(s.toString().toUpperCase());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                startActivity(intent);
                SearchActivity.this.finish();
            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JsonResponse jsonDoomie = new JsonResponse();
                jsonDoomie.setNombre_municipio(txtSearch.getText().toString());

                Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                intent.putExtra("data", gson.toJson(jsonDoomie));
                startActivity(intent);
                SearchActivity.this.finish();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void insertRealm(List<JsonResponse> data){

        RealmQuery<cityRealmModel> query = realm.where(cityRealmModel.class);

        query.equalTo("body", gson.toJson(data));
        cityRealmModel weatherModel = query.findFirst();

        if (weatherModel == null) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    Number maxID = realm.where(cityRealmModel.class).max("idModel");
                    int newKey = (maxID == null) ? 1 : maxID.intValue() + 1;
                    cityRealmModel cityRealmModel = realm.createObject(cityRealmModel.class, newKey);
                    Log.d("DATA>>>>", gson.toJson(data));
                    cityRealmModel.setBody(gson.toJson(data));

                }
            });
        }
    }

    public void  findChar(String secuense){

        listSend.clear();
        for (JsonResponse input:list){
            if(input.getNombre_municipio().contains(secuense)){
                listSend.add(input);
            }
        }

        if (listSend.size() == 0){ linearLayout.setVisibility(View.VISIBLE); }else{linearLayout.setVisibility(View.GONE);}

        recyclerViewSearch = new RecyclerViewSearch(listSend, this, this::close );
        recyclerView.setAdapter(recyclerViewSearch);
    }

    public void getData(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://inegifacil.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        InterfaceJsonCities interfaceJsonCities = retrofit.create(InterfaceJsonCities.class);
        Call<List<JsonResponse>> call = interfaceJsonCities.getCities("" + i);

        call.enqueue(new Callback<List<JsonResponse>>() {
            @Override
            public void onResponse(Call<List<JsonResponse>> call, Response<List<JsonResponse>> response) {

                if(!(response.isSuccessful())){
                    return;
                }

                for (JsonResponse item:response.body()){
                    if(item.getNombre_municipio() != "todo el estado"){
                        list.add(item);
                    }
                }

                if(i == 33){
                    progress.dismiss();
                    insertRealm(list);
                    return;
                }else{
                    i ++;
                    getData();
                }

            }

            @Override
            public void onFailure(Call<List<JsonResponse>> call, Throwable t) {
                Log.e("SEARCH ACTIVITY", t.toString());
            }
        });
    }

    public  void getLocalData(){

        cityRealmModel results = realm.where(cityRealmModel.class).findFirst();

        Type listType = new TypeToken<List<JsonResponse>>(){}.getType();
        List<JsonResponse> res = gson.fromJson(results.getBody() , listType );
        list = res;
        progress.dismiss();
    }


    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {

        RealmResults<cityRealmModel> results = realm.where(cityRealmModel.class).findAll();
        results.load();


        if(isConnected){
            lblGuardados.setVisibility(View.GONE);
            txtSearch.setEnabled(true);
            if(results.size() == 0){
                getData();
            }else{
                getLocalData();
            }
        }else{
            txtSearch.setEnabled(false);
            progress.dismiss();
            lblGuardados.setVisibility(View.VISIBLE);
            RealmResults<weatherModel> citys = realm.where(weatherModel.class).findAll();
            results.load();

            List<getWeatherModel> lst = new ArrayList<>();

            for (weatherModel item: citys){

                getWeatherModel obj = new getWeatherModel();

                Type listType = new TypeToken<List<itemListModel>>(){}.getType();
                List<itemListModel> res = gson.fromJson(item.getList() , listType );
                cityModel city = gson.fromJson(item.getCity(), cityModel.class);

                obj.setMessage(item.getMessage());
                obj.setList(res);
                obj.setCod(item.getCod());
                obj.setDate(item.getDate());
                obj.setCity(city);
                obj.setCnt(item.getCnt());

                lst.add(obj);
            }

            recyclerViewSearchOffline = new RecyclerViewAdapterSerachOffline(lst, this, this::closeOffline );
            recyclerView.setAdapter(recyclerViewSearchOffline);
        }
    }

    @Override
    public void close(JsonResponse data) {

        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
        intent.putExtra("data", gson.toJson(data));
        startActivity(intent);
        this.finish();

    }


    @Override
    public void closeOffline(getWeatherModel data) {

        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
        intent.putExtra("dataOffline", gson.toJson(data));
        startActivity(intent);
        this.finish();

    }
}
