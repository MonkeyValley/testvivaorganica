package com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model;

import java.util.List;

public class itemListModel {

    int dt;
    String dt_txt;
    List<itemWeatherModel> weather;
    mainModel main;

    public itemListModel() {
    }

    public itemListModel(int dt, String dt_txt, List<itemWeatherModel> weather, mainModel main) {
        this.dt = dt;
        this.dt_txt = dt_txt;
        this.weather = weather;
        this.main = main;
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public List<itemWeatherModel> getWeather() {
        return weather;
    }

    public void setWeather(List<itemWeatherModel> weather) {
        this.weather = weather;
    }

    public mainModel getMain() {
        return main;
    }

    public void setMain(mainModel main) {
        this.main = main;
    }
}
