package com.example.test_vivaorganica.Retrofit.GetWeatherLatLon;

import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.getWeatherModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface InterfaceJsonWeatherLatLon {

    @GET("data/2.5/forecast")
    Call<getWeatherModel> getWeatherLatLong(@Query("lat") String lat, @Query("lon") String lon, @Query("appid") String appid);

}
