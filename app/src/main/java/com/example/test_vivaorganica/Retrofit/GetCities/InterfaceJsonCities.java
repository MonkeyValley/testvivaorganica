package com.example.test_vivaorganica.Retrofit.GetCities;

import com.example.test_vivaorganica.Retrofit.GetCities.Model.JsonResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface InterfaceJsonCities {

    @GET("cities/{id}")
    Call<List<JsonResponse>> getCities(@Path("id") String id);
}
