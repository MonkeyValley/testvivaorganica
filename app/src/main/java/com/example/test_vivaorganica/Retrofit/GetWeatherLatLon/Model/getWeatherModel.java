package com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model;

import java.util.List;

public class getWeatherModel {

    String cod, date;
    int message, cnt;
    List<itemListModel> list;
    cityModel city;

    public getWeatherModel() {
    }

    public getWeatherModel(String date, String cod, int message, int cnt, List<itemListModel> list, cityModel city) {
        this.cod = cod;
        this.message = message;
        this.cnt = cnt;
        this.list = list;
        this.city = city;
        this.date = date;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public List<itemListModel> getList() {
        return list;
    }

    public void setList(List<itemListModel> list) {
        this.list = list;
    }

    public cityModel getCity() {
        return city;
    }

    public void setCity(cityModel city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
