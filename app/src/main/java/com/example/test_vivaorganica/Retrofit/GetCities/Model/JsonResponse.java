package com.example.test_vivaorganica.Retrofit.GetCities.Model;

public class JsonResponse {

    String id,
            clave_entidad,
            nombre_entidad,
            clave_municipio,
            nombre_municipio,
            clave_inegi,
            nombre_inegi,
            minx,
            miny,
            maxx,
            maxy,
            lat,
            lng;

    public JsonResponse() {
    }

    public JsonResponse(String id, String clave_entidad, String nombre_entidad, String clave_municipio, String nombre_municipio, String clave_inegi, String nombre_inegi, String minx, String miny, String maxx, String maxy, String lat, String lng) {
        this.id = id;
        this.clave_entidad = clave_entidad;
        this.nombre_entidad = nombre_entidad;
        this.clave_municipio = clave_municipio;
        this.nombre_municipio = nombre_municipio;
        this.clave_inegi = clave_inegi;
        this.nombre_inegi = nombre_inegi;
        this.minx = minx;
        this.miny = miny;
        this.maxx = maxx;
        this.maxy = maxy;
        this.lat = lat;
        this.lng = lng;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClave_entidad() {
        return clave_entidad;
    }

    public void setClave_entidad(String clave_entidad) {
        this.clave_entidad = clave_entidad;
    }

    public String getNombre_entidad() {
        return nombre_entidad;
    }

    public void setNombre_entidad(String nombre_entidad) {
        this.nombre_entidad = nombre_entidad;
    }

    public String getClave_municipio() {
        return clave_municipio;
    }

    public void setClave_municipio(String clave_municipio) {
        this.clave_municipio = clave_municipio;
    }

    public String getNombre_municipio() {
        return nombre_municipio;
    }

    public void setNombre_municipio(String nombre_municipio) {
        this.nombre_municipio = nombre_municipio;
    }

    public String getClave_inegi() {
        return clave_inegi;
    }

    public void setClave_inegi(String clave_inegi) {
        this.clave_inegi = clave_inegi;
    }

    public String getNombre_inegi() {
        return nombre_inegi;
    }

    public void setNombre_inegi(String nombre_inegi) {
        this.nombre_inegi = nombre_inegi;
    }

    public String getMinx() {
        return minx;
    }

    public void setMinx(String minx) {
        this.minx = minx;
    }

    public String getMiny() {
        return miny;
    }

    public void setMiny(String miny) {
        this.miny = miny;
    }

    public String getMaxx() {
        return maxx;
    }

    public void setMaxx(String maxx) {
        this.maxx = maxx;
    }

    public String getMaxy() {
        return maxy;
    }

    public void setMaxy(String maxy) {
        this.maxy = maxy;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
