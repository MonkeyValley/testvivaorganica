package com.example.test_vivaorganica.Retrofit.GetCityByName;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.getWeatherModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface InterfaceJsonCityName {
    @GET("http://api.openweathermap.org/data/2.5/forecast")
    Call<getWeatherModel> getCitieByName(@Query("q") String cityName, @Query("appid") String appid);
}
