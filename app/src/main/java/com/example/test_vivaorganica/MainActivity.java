package com.example.test_vivaorganica;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.example.test_vivaorganica.Adapters.ReciclerViewAdapterDays;
import com.example.test_vivaorganica.Adapters.RecyclerViewAdapterHeader;
import com.example.test_vivaorganica.Helpers.MainCallback;
import com.example.test_vivaorganica.Helpers.SearchCallback;
import com.example.test_vivaorganica.LocalModel.weatherModel;
import com.example.test_vivaorganica.Retrofit.GetCities.Model.JsonResponse;
import com.example.test_vivaorganica.Retrofit.GetCityByName.InterfaceJsonCityName;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.InterfaceJsonWeatherLatLon;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.cityModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.getWeatherModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.itemListModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.mainModel;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.itemWeatherModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Converter.*;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class MainActivity extends AppCompatActivity implements InternetConnectivityListener {

    private RecyclerView recyclerView, recyclerViewHeader;
    private ReciclerViewAdapterDays reciclerViewAdapterDays;
    private RecyclerViewAdapterHeader recyclerViewAdapterHeader;
    private FusedLocationProviderClient client;


    Button btnSearch, ic_location;
    LottieAnimationView lt;
    LinearLayout msjNoDta, lastActualzation, linearnoData;
    TextView lblGrados, lblTiempo, lblCiudad, lblDate;
    List <itemListModel> listDays;
    Gson gson;
    Realm realm;
    getWeatherModel jsonList;
    InternetAvailabilityChecker mInternetAvailabilityChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


// INICIALIZACION DE VARIABLES
        recyclerView =  findViewById(R.id.recycler_view_days);
        recyclerViewHeader =  findViewById(R.id.recycler_view_header);
        msjNoDta = findViewById(R.id.layout_no_data);
        lt = findViewById(R.id.lotie_animation);
        lblGrados = findViewById(R.id.lblGrados);
        lblCiudad = findViewById(R.id.lblCiudad);
        lblTiempo = findViewById(R.id.lblTiempo);
        btnSearch = findViewById(R.id.btnSearchCity);
        ic_location = findViewById(R.id.locationIc);
        lblDate = findViewById(R.id.lblLastActualization);
        lastActualzation = findViewById(R.id.linearLastActualizacion);
        linearnoData = findViewById(R.id.linearnoData);

        gson = new Gson();
        Realm.init(this);
        realm = Realm.getDefaultInstance();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewHeader.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


//ASIGNACION DE LISTENER A LOS BOTONES
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                MainActivity.this.finish();
            }
        });

//SOLICITUD DE PERMISOS PARA LA GEOLOCALIZACION
        requestPermissions();

        if(ActivityCompat.checkSelfPermission(MainActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            return;
        }

        //INICIALIZACION DEL LISTENER PARA LA CONEXION A INTERNET
        InternetAvailabilityChecker.init(this);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

//*************** FUNCIONES DE CONFIGURACIONES Y ASIGNACION DE DATOS ***************

    private  void  requestPermissions(){
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
    }

//FUNCION LISTENER QUE DETECTA UN CAMBIO EN LA CONEXION A INTERNET
    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {

        if(isConnected){
            if(getIntent().getStringExtra("data") != null){

                JsonResponse jsonResponse = gson.fromJson( getIntent().getStringExtra("data"), JsonResponse.class);
                this.intetData(jsonResponse);

            }else{
                client = LocationServices.getFusedLocationProviderClient(this);
                client.getLastLocation().addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if(location != null){
                            getWeatherLatLon(location.getLatitude()+"" , location.getLongitude()+"");
                        }
                    }
                });
            }

        }else{

            if(getIntent().getStringExtra("dataOffline") != null){

                getWeatherModel getWeatherModel = gson.fromJson( getIntent().getStringExtra("dataOffline"), getWeatherModel.class);
                this.asignedData(getWeatherModel, true);

            }else{


                weatherModel item = realm.where(weatherModel.class).findFirst();
                getWeatherModel obj = new getWeatherModel();

                if(jsonList != null){
                    RealmQuery<weatherModel> query = realm.where(weatherModel.class);
                    query.equalTo("city", gson.toJson(jsonList.getCity()));
                    item = query.findFirst();
                }

                if( item != null ) {
                    cityModel city = gson.fromJson(item.getCity(), cityModel.class);

                    Type listType = new TypeToken<List<itemListModel>>(){}.getType();
                    List<itemListModel> list = gson.fromJson( item.getList() ,  listType);

                    obj.setCnt( item.getCnt());
                    obj.setCity(city);
                    obj.setCod(item.getCod());
                    obj.setList(list);
                    obj.setMessage(item.getMessage());

                    asignedData(obj, true);

                }else{
                    setEmptyData();
                }
            }

        }
    }


//FUNCION PARA LA ASIGNACION DE LOS DATOS PARA LA VISTA (LABELS, ANIMACIONES Y RECYCLERSVIEWS)
    public void  asignedData(getWeatherModel jsonList, boolean local){

        List <itemListModel> listHeader = new ArrayList<itemListModel>();
        listDays = new ArrayList<>();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(Calendar.getInstance().getTime());

        jsonList.setDate(date);

        itemListModel firstItem = jsonList.getList().get(0);

        lblTiempo.setText( getStringResourceByName(firstItem.getWeather().get(0).getMain()) );
        double mathGrades = firstItem.getMain().getTemp() - 273.15;
        lblGrados.setText( (int) mathGrades +"º");
        lblCiudad.setText(jsonList.getCity().getName());

        int idTemp = firstItem.getWeather().get(0).getId();
        if (idTemp < 599 && idTemp >= 200){
            lt.setAnimation(R.raw.rain);
        }else if (idTemp < 805 && idTemp >= 801){
            lt.setAnimation(R.raw.suncloud);
        } else {
            lt.setAnimation(R.raw.sun);
        }
        lt.playAnimation();


        for (int i=0;i<jsonList.getList().size();i++){
            itemListModel item = jsonList.getList().get(i);
            String[] strSplit = item.getDt_txt().split(" ");
            if(date.equals(strSplit[0])){
                listHeader.add(item);
            }else{

                String[] dateItem  = item.getDt_txt().split(" ");

                if( listDays.size() == 0){
                    listDays.add(item);
                }else{

                    itemListModel itm = listDays.get(listDays.size() - 1);

                    if (!itm.getDt_txt().split(" ")[0].equals(item.getDt_txt().split(" ")[0])) {
                        listDays.add(item);
                    }
                }
            }
        }

        if(!local) {
            lastActualzation.setVisibility(View.GONE);
            insertRealm(jsonList);
        }else{
            lastActualzation.setVisibility(View.VISIBLE);
            lblDate.setText(jsonList.getDate());
        }

        linearnoData.setVisibility( View.VISIBLE );
        msjNoDta.setVisibility( View.GONE );
        ic_location.setVisibility(View.VISIBLE);
        reciclerViewAdapterDays = new ReciclerViewAdapterDays(listDays, this);
        recyclerViewAdapterHeader = new RecyclerViewAdapterHeader(listHeader);
        recyclerView.setAdapter(reciclerViewAdapterDays);
        recyclerViewHeader.setAdapter(recyclerViewAdapterHeader);
    }

//FUNCION LISTENER PARA QUE DETECTA LA ELECCION DE LOS PERMISOS

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            InternetAvailabilityChecker.init(this);
            mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
            mInternetAvailabilityChecker.addInternetConnectivityListener(this);
        }

    }


//FUNCION HELPER PARA LA ASIGNACION DE DATOS EN CASO DE QUE NO HAYA DATOS PARA MOSTRAR
    public  void  setEmptyData(){
        linearnoData.setVisibility( View.GONE );
        msjNoDta.setVisibility( View.VISIBLE );
        ic_location.setVisibility(View.GONE);
        lblTiempo.setText("");
        lblCiudad.setText("");
    }

//FUNCION PARA OBTENER UN STRING POR MEDIO DE LOS RECURSOS
    private String getStringResourceByName(String aString) {
        String packageName = getPackageName();
        int resId = getResources().getIdentifier(aString, "string", packageName);
        return getString(resId);
    }

// *************** FUNCIONES DE ONLINE ***************

//OBTENCION DEL CLIMA POR MEDIO DE LATITUD Y LONGITUD
    public void getWeatherLatLon(String latitud, String longitud){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        InterfaceJsonWeatherLatLon interfaceJsonWeatherLatLon = retrofit.create(InterfaceJsonWeatherLatLon.class);
        Call<getWeatherModel> call = interfaceJsonWeatherLatLon.getWeatherLatLong(latitud, longitud, "bc1ac6140319ee7ee6710e5ae5b8fc6f");
        //Call<getWeatherModel> call = interfaceJsonWeatherLatLon.getWeatherLatLong();

        call.enqueue(new Callback<getWeatherModel>() {
            @Override
            public void onResponse(Call<getWeatherModel> call, Response<getWeatherModel> response) {
                Log.d("MAIN ACTIVITY", response.toString());
                if(!(response.isSuccessful())){
                    setEmptyData();
                    return;
                }
                jsonList = response.body();

                if(jsonList.getList().size() == 0){
                    setEmptyData();
                    return;
                }
                asignedData(jsonList, false);
            }

            @Override
            public void onFailure(Call<getWeatherModel> call, Throwable t) {
                Log.e("MAIN ACTIVITY", t.toString());
                setEmptyData();
            }
        });
    }

//OBTENCION DE DATOS DEL CLIMA PAOR MEDIO DEL NOMBRE DE LA CIUDAD
    public void intetData(JsonResponse response){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        InterfaceJsonCityName interfaceJsonCityName = retrofit.create(InterfaceJsonCityName.class);
        Call<getWeatherModel> call = interfaceJsonCityName.getCitieByName( response.getNombre_municipio() ,"bc1ac6140319ee7ee6710e5ae5b8fc6f");

        call.enqueue(new Callback<getWeatherModel>() {
            @Override
            public void onResponse(Call<getWeatherModel> call, Response<getWeatherModel> response) {

                Log.d("MAIN ACTIVITY", response.toString());
                if(!(response.isSuccessful())) {
                    setEmptyData();
                    return;
                }

                getWeatherModel jsonList = response.body();

                if(jsonList.getList().size() == 0){
                    setEmptyData();
                    return;
                }

                asignedData(jsonList, false);
            }

            @Override
            public void onFailure(Call<getWeatherModel> call, Throwable t) {
                Log.e("MAIN ACTIVITY", t.toString());
            }
        });
    }

    // *************** FUNCIONES DE OFFLINE ***************


// INSERCION DE DATOS A LA BASE DE DATOS LOCAL POR MEDIO DE REALM
    public void insertRealm(getWeatherModel jsonList){

        RealmQuery<weatherModel> query = realm.where(weatherModel.class);

        query.equalTo("city", gson.toJson(jsonList.getCity()) );
        weatherModel weatherModel = query.findFirst();

        if (weatherModel == null) {
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {

                    Number maxID = bgRealm.where(weatherModel.class).max("id");
                    int newKey = (maxID == null) ? 1 : maxID.intValue() + 1;
                    weatherModel weatherItem = bgRealm.createObject(weatherModel.class, newKey);

                    weatherItem.setCity(gson.toJson(jsonList.getCity()));
                    weatherItem.setCnt(jsonList.getCnt());
                    weatherItem.setCod(jsonList.getCod());
                    weatherItem.setList(gson.toJson(jsonList.getList()));
                    weatherItem.setDate(jsonList.getDate());
                    weatherItem.setMessage(jsonList.getMessage());

                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {

                    Log.d("SAVED ->", jsonList.getCity().getName());
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    // Transaction failed and was automatically canceled.
                    Log.e("MAIN ACTIVITY", error.toString());
                }
            });
        }else{
            if(!weatherModel.getDate().equals(jsonList.getDate())){
                realm.beginTransaction();
                weatherModel.setMessage(jsonList.getMessage());
                weatherModel.setList(gson.toJson(jsonList.getList()));
                weatherModel.setCod(jsonList.getCod());
                weatherModel.setCnt(jsonList.getCnt());
                weatherModel.setCity(gson.toJson(jsonList.getCity()));
                weatherModel.setDate(jsonList.getDate());
                realm.commitTransaction();
            }
        }
    }

}
