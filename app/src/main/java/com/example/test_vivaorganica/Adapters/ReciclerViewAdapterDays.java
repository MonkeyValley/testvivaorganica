package com.example.test_vivaorganica.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.itemListModel;
import com.example.test_vivaorganica.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReciclerViewAdapterDays extends RecyclerView.Adapter<ReciclerViewAdapterDays.ViewHolder> {

    public List<itemListModel> lista;
    public Context ctx;

    public  static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView fecha, titulo, grados;
        private ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);

            fecha = itemView.findViewById(R.id.lblDateDays);
            titulo = itemView.findViewById(R.id.lblTiempoDays);
            grados = itemView.findViewById(R.id.lblGradesDays);
            img = itemView.findViewById(R.id.iconDays);

        }
    }

    public ReciclerViewAdapterDays(List<itemListModel> lista, Context ctx) {
        this.lista = lista;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent,false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = df.parse(lista.get(position).getDt_txt().split(" ")[0]);
            String monthname=(String)android.text.format.DateFormat.format("MMM", date);
            String numday=(String)android.text.format.DateFormat.format("d", date);

            holder.fecha.setText( monthname + " " + numday);
        } catch (ParseException ex) {
            Log.e("Exception", ex.getLocalizedMessage());
        }

        double mathGrades = lista.get(position).getMain().getTemp() - 273.15;
        holder.titulo.setText( getStringResourceByName(lista.get(position).getWeather().get(0).getMain()));

        holder.grados.setText((int) mathGrades + "º");



        int idTemp = lista.get(position).getWeather().get(0).getId();

        if (idTemp < 599 && idTemp >= 200){
            holder.img.setImageResource(R.drawable.ic_rain);
        }else if (idTemp < 805 && idTemp >= 801){
            holder.img.setImageResource(R.drawable.ic_suncloud);
        } else {
            holder.img.setImageResource(R.drawable.ic_sun);
        }

        //holder.img.setText(lista.get(position).getDt_txt());

    }

    private String getStringResourceByName(String aString) {
        String packageName = ctx.getPackageName();
        int resId = ctx.getResources().getIdentifier(aString, "string", packageName);
        return ctx.getString(resId);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }
}
