package com.example.test_vivaorganica.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test_vivaorganica.Helpers.SearchCallback;
import com.example.test_vivaorganica.R;
import com.example.test_vivaorganica.Retrofit.GetCities.Model.JsonResponse;
import com.example.test_vivaorganica.Retrofit.GetCityByName.InterfaceJsonCityName;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.InterfaceJsonWeatherLatLon;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.getWeatherModel;
import com.example.test_vivaorganica.SearchActivity;
import com.google.android.gms.vision.text.Line;

import org.w3c.dom.Text;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecyclerViewSearch extends RecyclerView.Adapter<RecyclerViewSearch.ViewHolder>{

    public List<JsonResponse> lista;
    public Context ctx;
    public SearchCallback listener;

    public  static class ViewHolder extends RecyclerView.ViewHolder{

        TextView lblName, lblDesc;
        LinearLayout click;

        public ViewHolder(View itemView) {
            super(itemView);
            lblName = itemView.findViewById(R.id.lblNameCity);
            lblDesc = itemView.findViewById(R.id.lbldescSearch);
            click = itemView.findViewById(R.id.clickItemSearch);
        }
    }

    public RecyclerViewSearch(List<JsonResponse> lista, Context ctx, SearchCallback listener) {
        this.lista = lista;
        this.ctx = ctx;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent,false);
        RecyclerViewSearch.ViewHolder viewHolder = new RecyclerViewSearch.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.lblName.setText(lista.get(position).getNombre_municipio() + ", ");
        holder.lblDesc.setText(lista.get(position).getNombre_entidad());

        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.close(lista.get(position));
            }
        });


    }

    @Override
    public int getItemCount() {
        return lista.size();
    }


}

