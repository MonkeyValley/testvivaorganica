package com.example.test_vivaorganica.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test_vivaorganica.Helpers.SearchCallback;
import com.example.test_vivaorganica.Helpers.SearchOfflineCallback;
import com.example.test_vivaorganica.R;
import com.example.test_vivaorganica.Retrofit.GetCities.Model.JsonResponse;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.getWeatherModel;

import java.util.List;

public class RecyclerViewAdapterSerachOffline extends RecyclerView.Adapter<RecyclerViewAdapterSerachOffline.ViewHolder>{

    public List<getWeatherModel> lista;
    public Context ctx;
    public SearchOfflineCallback listener;

    public  static class ViewHolder extends RecyclerView.ViewHolder{

        TextView lblName, lblDesc;
        LinearLayout click;

        public ViewHolder(View itemView) {
            super(itemView);
            lblName = itemView.findViewById(R.id.lblNameCity);
            lblDesc = itemView.findViewById(R.id.lbldescSearch);
            click = itemView.findViewById(R.id.clickItemSearch);
        }
    }

    public RecyclerViewAdapterSerachOffline(List<getWeatherModel> lista, Context ctx, SearchOfflineCallback listener) {
        this.lista = lista;
        this.ctx = ctx;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent,false);
        RecyclerViewAdapterSerachOffline.ViewHolder viewHolder = new RecyclerViewAdapterSerachOffline.ViewHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.lblName.setText(lista.get(position).getCity().getName() + ",");
        holder.lblDesc.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
        holder.lblDesc.setTextSize(12);
        holder.lblDesc.setText(" offline (" + lista.get(position).getDate()+")");
        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.closeOffline(lista.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }




}
