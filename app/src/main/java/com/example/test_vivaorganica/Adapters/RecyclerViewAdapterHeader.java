package com.example.test_vivaorganica.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.itemListModel;
import com.example.test_vivaorganica.R;

import java.util.List;

public class RecyclerViewAdapterHeader extends RecyclerView.Adapter<RecyclerViewAdapterHeader.ViewHolder> {


    public List<itemListModel> lista;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView grados, hora;


        public ViewHolder(View itemView) {
            super(itemView);

            grados = itemView.findViewById(R.id.lblGradesHeader);
            hora = itemView.findViewById(R.id.lblTimeHeader);
        }
    }


    public RecyclerViewAdapterHeader(List<itemListModel> lista) {
        this.lista = lista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent,false);
        RecyclerViewAdapterHeader.ViewHolder viewHolder = new RecyclerViewAdapterHeader.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        double mathGrades = lista.get(position).getMain().getTemp() - 273.15;
        String[] date = lista.get(position).getDt_txt().split(" ");
        String[] hour = date[1].split(":");
        holder.grados.setText((int) mathGrades + "º");
        holder.hora.setText(hour[0]+":"+hour[1]);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }
}
