package com.example.test_vivaorganica.Helpers;

import com.example.test_vivaorganica.Retrofit.GetCities.Model.JsonResponse;

public interface MainCallback {

    void closeMain(JsonResponse data);
}
