package com.example.test_vivaorganica.Helpers;

import com.example.test_vivaorganica.Retrofit.GetCities.Model.JsonResponse;
import com.example.test_vivaorganica.Retrofit.GetWeatherLatLon.Model.getWeatherModel;

public interface SearchCallback {
    void close(JsonResponse data);
}
